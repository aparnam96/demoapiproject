/**
 * constant
 *
 * @author aparna
 * @date 9/20/19
 **/
component  {

/*----------------------------------------------------------------------------------
    Function Name : init
    Description : This funnction returns digital signature key.
    Arguments : none
    Return Type : String
    ----------------------------------------------------------------------------------*/
	function init()
	{
		variables.secret = "test123";
		return this;
	}

}