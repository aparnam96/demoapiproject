/**
 * TokenAuthorizationService
 *
 * @author aparna
 * @date 11/2/19
 **/
component accessors=true output=false persistent=false {

 /*----------------------------------------------------------------------------------
    Function Name : CheckBlacklistToken
    Description : Check whether token exist in blacklist or not
    Arguments : token
    Return Type : Boolean
    ----------------------------------------------------------------------------------*/
    public function CheckBlacklistToken(token){
	  	try{

		  	var isExist = false;
		  	var queryObj = new Query();
		  	queryObj.addParam(name="token" ,value=ARGUMENTS.token ,cfsqltype="varchar");
		  	var queryResult = queryObj.execute(sql = "SELECT Token FROM BlacklistTokens WHERE Token =:token");
		  	if(queryResult.getPrefix().recordcount >= 1)
		  	{
		  		isExist = true;
		  	}
	    }
	    catch(any exception)
	    {
	        WriteLog(type="Error", file="APIError.log",
	        text="TYPE : [#exception.type#] ;MESSAGE: #exception.message#;DETAIL: #exception.detail#");
	    }
	    return isExist;
  }

}