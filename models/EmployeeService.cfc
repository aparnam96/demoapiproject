/**
* I am a new Model Object
*/
component accessors="true"{

	/*----------------------------------------------------------------------------------
    Function Name : GetEmployeeDetail
    Description : Get employee detail.
    Arguments : id
    Return Type : Struct
    ----------------------------------------------------------------------------------*/
	public function GetEmployeeDetail(id){
        try{
	        //This variables hold employee detail
		    var empStruct = StructNew();
		    empStruct.error = false;
		    var empDetailStruct = StructNew();
		    //Create Query Object
			var queryObj = new Query();
			//Creating the query accorging to id.
			var baseSql = "SELECT * FROM Employee";
			if(ARGUMENTS.id)
			{
				baseSql &= " WHERE EmpID = #ARGUMENTS.id#";
			}

			var  queryResult = queryObj.execute(sql=baseSql);
			var  empDetailQuery = queryResult.getResult();
			if(queryResult.getPrefix().recordcount>=1)
			{
		    	var  i = 1;
		        for (row in empDetailQuery)
		        {
			        empDetailStruct[ i ]["EmpId"] = row.EmpID;
			        empDetailStruct[ i ]["EmployeeName"] = row.Name;
			        empDetailStruct[ i ]["Email"] = row.Email;
			        empDetailStruct[ i ]["Skills"] = row.Skills;
			        empDetailStruct[ i ]["Salary"] = row.Salary;
		            i++;
			    }
			   empStruct.data = empDetailStruct;
	        }
	        else
	        {
			    empStruct.error = true;
			    empStruct.message = "Either the id is incorrect or no data is present in db";
	        }
       }
       catch(any exception)
       {
       	  empStruct.error = true;
   	      empStruct.message ="Server Error";
   	      WriteLog(type="Error", file="APIError.log",
          text="TYPE : [#exception.type#] ;MESSAGE: #exception.message#;DETAIL: #exception.detail#");
       }
       return empStruct;
	}
   /*----------------------------------------------------------------------------------
    Function Name : ValidateEmployee
    Description : vildate the employee form fields
    Arguments : rc,detailKeyArray
    Return Type : Boolean
    ----------------------------------------------------------------------------------*/
       function ValidateEmployee(rc,detailKeyArray)
       {
	       var valid = true;
	       for(var i =1 ;i<=ArrayLen(detailKeyArray);i++)
	       {
			   if(!StructKeyExists(rc,ARGUMENTS.detailKeyArray[i]) || len(trim(rc["#ARGUMENTS.detailKeyArray[i]#"])) <=0)
			   {
			       valid = false;
			       return valid;
			   }
	       }
	       return valid;
       }
    /*----------------------------------------------------------------------------------
    Function Name : AddEmployee
    Description : add new employee details to db
    Arguments : rc
    Return Type : Struct
    ----------------------------------------------------------------------------------*/
    public function AddEmployee(rc){
	   	try{
	   		var result = StructNew();
	   		var keyArray = ["EmpName","EmpEmail","EmpSkills","EmpSalary"];
	   		//checks for all details exist or not.
	   		var valid = ValidateEmployee(rc, keyArray)
	   		if(valid)
	   		{
			   	queryObj = new Query();
	           //query Parameter
	            queryObj.addParam(name="Name",  value=rc.EmpName, cfsqltype="VARCHAR");
	            queryObj.addParam(name="Email", value=rc.EmpEmail, cfsqltype="VARCHAR");
	            queryObj.addParam(name="Skills", value=rc.EmpSkills, cfsqltype="VARCHAR");
	            queryObj.addParam(name="Salary", value=rc.EmpSalary, cfsqltype="VARCHAR");
	            var queryResult = queryObj.execute(sql="INSERT INTO Employee
	                                                (Name, Email, Skills, Salary)
	                                                VALUES(:Name,:Email, :Skills, :Salary)");
	            var empQuery = queryResult.getResult();
	            var count = Queryresult.getPrefix();
	            if(count.RecordCount == 1)
	            {
	            	result.data = "";
	            	result.message = "Employee Detail is added successfully" ;
	            	result.error = false
	            }
            }
            else
            {
            	result.data = "";
            	result.message = "Invalid or Incomplete Details of Employee";
            	result.error = true;
            }
           }
           catch(any exception)
           {
	           	result.data = "";
	           	result.message ="Server Error";
	           	result.error = true;
	           	WriteLog(type="Error", file="APIError.log",
	            text="TYPE : [#exception.type#] ;MESSAGE: #exception.message#;DETAIL: #exception.detail#");
           }
           return result;
	}

 /*----------------------------------------------------------------------------------
    Function Name : UpdateEmployee
    Description : update employee details in db
    Arguments : rc
    Return Type : Struct
    ----------------------------------------------------------------------------------*/
    public function UpdateEmployee(rc){
	   try{
		   	   var result = StructNew();
		   	   queryObj = new Query();
               queryObj.addParam(name="ID",  value=rc.EmpID, cfsqltype="INTEGER");
               queryObj.addParam(name="Name",  value=rc.EmpName, cfsqltype="VARCHAR");
               queryObj.addParam(name="Email", value=rc.EmpEmail, cfsqltype="VARCHAR");
               queryObj.addParam(name="Skills", value=rc.EmpSkills, cfsqltype="VARCHAR");
               queryObj.addParam(name="Salary", value=rc.EmpSalary, cfsqltype="VARCHAR");
               var  queryResult = queryObj.execute(sql="UPDATE Employee
				                                        SET Name = :Name,
				                                        Email =:Email,
				                                        Skills=:Skills,
				                                        Salary =:Salary
				                                        WHERE EmpID= :ID");
               var contactQuery = queryresult.getResult();
               var count = queryresult.getPrefix().recordCount;
               if(count > 0)
               {
                  result.message = "Employee Detail is updated";
                  result.error = false;
               }
               else
               {
                  result.message = "Employee ID does not Exist";
                  result.error = true;
               }
		   }
		   catch(any exception)
		   {
	   	    	result.message = "Database error occured";
	   	    	result.error = true;
	   	    	WriteLog(type="Error", file="APIError.log",
                text="TYPE : [#exception.type#] ;MESSAGE: #exception.message#;DETAIL: #exception.detail#");
		   }
           return result;
	}



	 /*----------------------------------------------------------------------------------
    Function Name : Remove
    Description : Remove employee from db
    Arguments : id
    Return Type : Struct
    ----------------------------------------------------------------------------------*/
    public function Remove( ID ){
    	//this variable will holds the result
		var  result = StructNew();
		result.data = "";
		result.message = "";
		result.error = false;
        try{
	        var queryObj = new Query();
	        queryObj.addParam(name="ID",value="#ARGUMENTS.ID#",cfsqltype="NUMERIC");
	        var  queryResult = queryObj.execute(sql="DELETE FROM Employee WHERE EmpID = :ID");
	        var  count = Queryresult.getPrefix().recordCount;
	        if(count>0){
	            result.message = "Employee Detail is deleted successfully";
	            result.error = false;
	        }
           else
            {
                result.message = "Employee ID does not exist";
                result.error = true;
            }
       	 }
         catch(any exception)
         {
	     	 result.message = "Server error occured";
	         result.error = true;
	         WriteLog(type="Error", file="APIError.log",
	         text="TYPE : [#exception.type#] ;MESSAGE: #exception.message#;DETAIL: #exception.detail#");

         }
		 return result;
    }

}