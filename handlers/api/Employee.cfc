/**
* I am a new handler
*/
component displayname="Employee"
          hint="This is a handler for employee section."
          extends= "handlers.BaseHandler"{
    // Dependency Injection
    property name="employeeService" inject;

	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		View   ='GET',
		Remove ='DELETE',
		Add    ='POST',
		Update ='PUT'
	};



	/*----------------------------------------------------------------------------------
    Function Name : View
    Description : Return all information of employees
    Arguments : event, rc, prc
    Return Type : JSON
    ----------------------------------------------------------------------------------*/
	public void function View( event, rc, prc ){
		var id = 0;
		if(structkeyexists(rc,"id"))
		{
			 id = rc.id;
		}
		var result = employeeService.GetEmployeeDetail(id);
		if(result.error == false)
		{
			prc.response.setData( result.data );
		}
		else if(result.message == "Server Error")
		{
			 prc.response.setError(true)
                .setStatusCode(STATUS.INTERNAL_ERROR)
                .setStatusText("Server Error")
                .addMessage("Server Error Occured");
		}
		else{
			 prc.response.setError(true)
			 .addMessage(result.message);
		}
	}

	/*----------------------------------------------------------------------------------
    Function Name : View
    Description : Add new employee
    Arguments : event, rc, prc
    Return Type : JSON
    ----------------------------------------------------------------------------------*/
	 public void function Add(event, rc, prc){
	 	var isAdded = employeeService.AddEmployee(rc)
	 	if(isAdded.message == "Server Error")
	 	{
	 		prc.response.setError(true)
                .setStatusCode(STATUS.INTERNAL_ERROR)
                .setStatusText("Server Error")
                .addMessage("Server Error Occured");
	 	}
	 	else
	 	{
	 	    prc.response.setError(isAdded.error).addMessage(isAdded.message);
	    }
	 }

	/*----------------------------------------------------------------------------------
    Function Name : Update
    Description : Update employee details.
    Arguments : event, rc, prc
    Return Type : JSON
    ----------------------------------------------------------------------------------*/
	public void function Update( event, rc, prc ){

	    var isUpdated = employeeService.UpdateEmployee(rc);
	    if(isUpdated.message == "Server Error")
	    {
	    	prc.response.setError(true)
                .setStatusCode(STATUS.INTERNAL_ERROR)
                .setStatusText("Server Error")
                .addMessage("Server Error Occured");
	    }
	    else
	    {
	    	 prc.response.setError(isUpdated.error).addMessage(isUpdated.message);
	    }

	}

	/*----------------------------------------------------------------------------------
    Function Name : Remove
    Description : Delete existing emplyee.
    Arguments : event, rc, prc
    Return Type : JSON
    ----------------------------------------------------------------------------------*/
	public void function Remove( event, rc, prc ){
        var id = 0;
        if(structkeyexists(rc,"id"))
        {
             id = rc.id;
        }
	    var isDeleted = employeeService.Remove(id);
	    if(isDeleted.message == "Server Error")
	    {
	    	prc.response.setError(true)
                .setStatusCode(STATUS.INTERNAL_ERROR)
                .setStatusText("Server Error")
                .addMessage("Server Error Occured");
	    }
	    else{
	    	prc.response.setError(isDeleted.error).addMessage(isUpdated.message);
	    }

	}


}
