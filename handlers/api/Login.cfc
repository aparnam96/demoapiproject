/**
 * login
 *
 * @author aparna
 * @date 9/19/19
 **/
component displayname="Login"
          hint="This is a handler for Login section."
          extends="handlers.BaseHandler"{

	property name="authenticationService" inject;

    //REST Allowed methods
    this.allowedMethods = {
		CheckCredential = 'POST'

	};


   /*----------------------------------------------------------------------------------
    Function Name : CheckCredentials
    Description : Checks username and password, if exist return token else return 401 error.
    Arguments : event, rc, prc
    Return Type : JSON
    ----------------------------------------------------------------------------------*/
     public void function CheckCredential(event, rc, prc,eventArguments){
	   	//Access data from request payload.
	   	//var requestBody =  deserializeJSON(toString(getHttpRequestData().content));
	   	//Append to rc struc
	    //StructAppend(rc,requestBody);

	    //this variable holds result
	   	var result = StructNew();
	   	//call the service to check with database.
	    var user = authenticationService.DoLogin(rc.uName,rc.password);
	    //if logged in then return token
	   	if(user.isLoggedIn)
	   	{
	        result.token = user.token;
	   		prc.response.setData(result);
	   	}
	   	//else calls authentication failure method of base handler.
	   	else
	   	{
	        onAuthenticationFailure();
	   	}
    }

}