component{

	function configure(){
		// Set Full Rewrites
		setFullRewrites( true );

		/**
		 * --------------------------------------------------------------------------
		 * App Routes
		 * --------------------------------------------------------------------------
		 * Here is where you can register the routes for your web application!
		 **/

        /*
         * routes for employee resource
         **/

		/*
		 * roues for view all employee or paticular employee through id
		 **/
        route("/api/employee/view/:id-numeric").toHandler("api.Employee.View");

        /*
         * route for adding new employee detail
         **/
        route("/api/employee/add").toHandler("api.Employee.Add");

        /*
         *route for updating detail of particular employee
         **/
        route("/api/employee/update").toHandler("api.Employee.Update");

        /*
         * route for deleting an employee detail
         **/
        route("/api/employee/remove/:id-numeric").toHandler("api.Employee.Remove");

        /*
         * route for registering api user.
         **/
        route(pattern = "/api/register/" ,target="api.Register.RegisterApiUser");
        /*
         * route for login to api
         **/
        route("/api/login").toHandler("api.Login.CheckCredential");

        /*
         * route for logout
         **/
        route("/api/logout").toHandler("api.Logout.DoLogout");

		/*
		 * A nice healthcheck route example
		 **/
		route("/abc",function(event,rc,prc){
			return "Ok!";
		});

		/*
		 * Conventions based routing
		 **/
		route( ":handler/:action?" ).end();
	}

}