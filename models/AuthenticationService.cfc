/**
 * loginAndToken
 *
 * @author aparna
 * @date 10/13/19
 **/
component displayname="Authentication Service"
          hint="This is a model for authentication."
           accessors=true output=false persistent=false {

  /*----------------------------------------------------------------------------------
    Function Name : RegisterUser
    Description : This functions add the user to database.
    Arguments : firstName, lastName, email, password
    Return Type : Struct
    ----------------------------------------------------------------------------------*/
    public struct function RegisterUser(string firstName, string lastName, string email, string password)
    {
       try{
       	  // this variable holds the result
    	   var result = StructNew();
    	   //checks whether email exist or not.
	       var isEmailExist = checkEmailExist(ARGUMENTS.email);
	       // if email do not exist in db then add register user to db.
	       if(!isEmailExist){

		       var queryObj = new Query();
		       queryObj.addParam(name = "firstName", value = ARGUMENTS.firstName, cfsqltype = "VARCHAR");
		       queryObj.addParam(name = "surName", value = ARGUMENTS.lastName, cfsqltype = "VARCHAR");
		       queryObj.addParam(name = "email", value = ARGUMENTS.email, cfsqltype = "VARCHAR");
		       queryObj.addParam(name = "password", value = ARGUMENTS.password, cfsqltype = "VARCHAR");
		       var insertUserQuery = queryObj.execute(sql = "INSERT INTO Person
		                                                     (FirstName, SurName, Email, Password)
		       	                                             VALUES(:firstName, :SurName, :email, :password)");
		       result.message = "User is successfully registered";
		       result.error = false;
	       }
	       //user exist then set error true.
	       else{
		       result.message ="You are already registered";
		       result.error = true;
	       }
    	}
    	catch(any exception)
    	{
	   		result.message="Server Error Occured";
	   		result.error = true;
	   		WriteLog(type="Error", file="APIError.log",
	           text="TYPE : [#exception.type#] ;MESSAGE: #exception.message#;DETAIL: #exception.detail#");
    	}
    	return result;
    }


   /*----------------------------------------------------------------------------------
    Function Name : CheckEmailExist
    Description : This functions check whether the email exist or not.
    Arguments : email
    Return Type : Struct
    ----------------------------------------------------------------------------------*/
    private boolean function CheckEmailExist(email)
    {
     try{
          var result = false;
	      var queryObj = new Query();
	      queryObj.addParam(name="email", value=ARGUMENTS.email, cfsqltype="VARCHAR");
	      var emailExistQuery = queryObj.execute(sql="SELECT Email FROM Person WHERE Email = :email");
	      if(emailExistQuery.getPrefix().recordcount ==1)
	      {
	      	result= true;
	      }
       }
       catch(any exception)
       {
            WriteLog(type="Error", file="APIError.log",
            text="TYPE : [#exception.type#] ;MESSAGE: #exception.message#;DETAIL: #exception.detail#");
       }
      return result;
    }


   /*----------------------------------------------------------------------------------
    Function Name : DoLogin
    Description : this function verifies email , password from the database if user exist it return a token.
    Arguments :email,password
    Return Type : Struct
    ----------------------------------------------------------------------------------*/
    public struct function DoLogin(email, password)
    {
	   	try
	   	{
		   	  var result = StructNew();
		   	  result.isLoggedIn = false;
		   	  var queryObj = new Query();
		   	  queryObj.addParam(name = "email", value = ARGUMENTS.email, cfsqltype = "VARCHAR");
		   	  queryObj.addParam(name = "password", value = ARGUMENTS.password, cfsqltype = "VARCHAR");
		   	  var userExistQuery = queryObj.execute(sql="SELECT PersonID,
		   	                                                    Email,
		   	                                                    Password
		   	                                             FROM Person
		   	                                             WHERE Email=:email AND Password=:password");
		   	  if(userExistQuery.getPrefix().recordcount == 1)
		   	  {
		   	  	  var loginInfo = userExistQuery.getResult();
		   	  	  //calls the generate token method.
		   	  	  result = GenerateToken(loginInfo.PersonId);
		   	  	  result.isLoggedIn = true;
		   	  }
	   	  }
	    catch(any exception)
	    {
	   	     WriteLog(type="Error", file="APIError1.log",
	         text="TYPE : [#exception.type#] ;MESSAGE: #exception.message#;DETAIL: #exception.detail#");
	   	}
	   	return result;
    }

   /*----------------------------------------------------------------------------------
    Function Name : GenerateToken
    Description : This function to generates the jwt token
    Arguments : userId
    Return Type : Struct
    ----------------------------------------------------------------------------------*/

    private struct function GenerateToken(userId)
    {
	  	 try
	  	 {
		  	  var date = now();
		      var extend = 30*60; //add 30 min for token expire.
		      var addTimeToDate = DateAdd("s",extend,date);
		      var expiryTokenTime = DateDiff("s",DateConvert("utc2Local", "January 1 1970 00:00"), addTimeToDate);
		        //create payload
		      var payload = {"ts" = date,"exp"= expiryTokenTime, "sub" = ARGUMENTS.userId};
		       // encode the payload.
		      var token = APPLICATION.JWT.encode(payload);
		      //send token and expiry time
		      var result = {"token": token, "expiryTime":extend};
	  	 }
	  	 catch( any exception)
	  	 {
	  	 	  WriteLog(type="Error", file="APIError.log",
	          text="TYPE : [#exception.type#] ;MESSAGE: #exception.message#;DETAIL: #exception.detail#");
	  	 }
	     return result;
   }

   /*----------------------------------------------------------------------------------
    Function Name : Dologout
    Description : This function performs logout by adding valid token to blacklist table in database.
    Arguments : validToken
    Return Type : Boolean
    ----------------------------------------------------------------------------------*/

    public boolean function Dologout(validToken)
    {
	  	try
	  	{
	  		 var result = false;
	  		 //verify the token.
	  		 var payload = APPLICATION.JWT.verify(ARGUMENTS.validToken);
	  		 //if token is valid then store to database
	  		 if(paload)
	  		 {
		  		 //token creation time.
		  		 var time = payload.ts;
			  	 var queryObj = new Query();
			  	 queryObj.addParam(name = "token", value = ARGUMENTS.validToken, cfsqltype = "varchar");
			  	 queryObj.addParam(name = "tokenCreationTime", value = time, cfsqltype = "time");
			  	 var queryResult = queryObj.execute(sql ="INSERT INTO BlacklistTokens
			  	                                          (Token,TokenCreationTime)
			  	                                          VALUES(:token, :tokenCreationTime)");
	  		 }
		  	 result = true;
	  	 }
	  	 catch(any exception)
	  	 {
	  		 WriteLog(type="Error", file="APIError.log",
	         text="TYPE : [#exception.type#] ;MESSAGE: #exception.message#;DETAIL: #exception.detail#");
	         result = false;

	  	 }
	  	 return result;
   }




}