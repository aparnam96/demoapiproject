/**
 * APISecurity
 *
 * @author aparna
 * @date 11/5/19
 **/
component accessors=true output=false persistent=false
    hint="Intercepts all the events for api and does security checks."{

   property name="checkTokenService" inject;
   property name="routingService" inject;

   function preProcess( event, interceptData, buffer )   {

    var HTTPVerb = event.getHTTPMethod();
    if(HTTPVerb == "OPTIONS")
    {
         event.overrideEvent('api.general.PassOptionRequest');
    }
    else
    {
         //get the routed url from event
	     var routedURL = event.getCurrentRoutedURL();
	     //check if route exist
	     var isRouteExist = routingService.CheckRoutes(routedURL, rc);



	     //if route is valid
	     if(isRouteExist)
	      {
	           //route is not for login or register then check for token.
	           if(routedURL != "api/login/" && routedURL != "api/register/")
	           {
	               //check for authorization parameter in the http header
	               if(StructkeyExists(GetHttpRequestData().headers,"Authorization"))
	               {
	                  //access token from the header
	                   var token= (ListLast(GetHttpRequestData().headers.authorization, " "));
	                   if(token != "")
	                   {
	                       try
	                        {
	                            //token is valid or not
	                            var payload = APPLICATION.JWT.decode(token);

                                var isExist = checkTokenService.CheckBlacklistToken(token);
                                //if exist in black list of tokens then throw error

                                if(isExist)
                                {
                                    throw(message="Invalid Token");
                                }
                                rc.payload = payload;
	                        }
	                        catch(any e)
	                        {   prc.message = "Invalid token";
	                            event.overrideEvent('BaseHandler.onTokenError');
	                        }
	                    }
	                    //if token is empty
	                    else
	                    {
                            prc.message="Invalid Access : No token is present in the header";
	                        event.overrideEvent('BaseHandler.onTokenError');
	                    }
	               }
	               //if header does not contain the authorization parameter
	               else
	               {
	                   event.overrideEvent( 'api.General.AuthorizationFailure' );
	               }

	          }
	        }
	        //if routed url is incorrect
	        else
	        {
	            event.overrideEvent('BaseHandler.routeNotFound');
	        }
	}

   }
}