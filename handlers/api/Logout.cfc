/**
 * logout
 *
 * @author aparna
 * @date 10/14/19
 **/
component displayname="DoLogout"
          hint="This is a handler for doing logout."
          extends= "handlers.BaseHandler" {

	property name="loginService" inject;

    //Rest allowed methods
	this.allowedMethods = {
		Dologout = 'GET'
	};

   /*----------------------------------------------------------------------------------
    Function Name : DoLogout
    Description : This funnction performs logout.
    Arguments : event, rc, prc
    Return Type : JSON
    ----------------------------------------------------------------------------------*/
	public void function DoLogout(event, rc, prc)
	{
        var result = loginService.DologoutAction(rc.token);
        if(result)
		{
			prc.response.setError(false).addMessage("Successfully logout");
		}
		else
		{
			prc.response.setError(true)
                .setStatusCode(STATUS.INTERNAL_ERROR)
                .setStatusText("Server Error")
                .addMessage("Server Error Occured");
		}

	}

}