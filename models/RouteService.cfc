/**
 * RouteService
 *
 * @author aparna
 * @date 11/4/19
 **/
component accessors=true output=false persistent=false {

     /*----------------------------------------------------------------------------------
    Function Name : CheckRoutes
    Description : Check the routed url is correct and have permission
    Arguments : routedURL,rc
    Return Type : Struct
    ----------------------------------------------------------------------------------*/
	public function CheckRoutes(routedURL, rc)
	{
		try{
			var result = false;
			//checks for routed URL
			switch(arguments.routedURL)
			{

				case "api/register/":
				case "api/login/":
				case "api/employee/view/":
				case "api/employee/update/":
				case "api/employee/remove/":
				       result = true;
				       break;
			}
	          //if routed url contains id.
			if(StructKeyExists(rc,"id"))
	        {
	        	addToUrl = rc.id&"/";
	            if("api/employee/view/"&addToUrl == routedURL )
	            {
	            	 result = true;
	            }
	            else if("api/employee/delete/"&addToUrl == routedURL)
	            {
	            	 result = true;
	            }
	        }
	   }
	   catch(any e)
	   {
	   	 WriteLog(type="Error", file="APIError.log",
            text="TYPE : [#exception.type#] ;MESSAGE: #exception.message#;DETAIL: #exception.detail#");
	   }
		return result;
	}

}