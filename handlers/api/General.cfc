/**
 * general
 *
 * @author aparna
 * @date 9/17/19
 **/
component displayname="General"
          hint="This is a handler for common error handling methods."
          extends="handlers.BaseHandler"{


   /*----------------------------------------------------------------------------------
	Function Name : PassOptionRequest
	Description : Return empty data to options request
	Arguments : event, rc, prc
	Return Type : JSON
	----------------------------------------------------------------------------------*/
   public void function PassOptionRequest(event, rc, prc)
   {

       prc.response.setData("");

   }

   /*----------------------------------------------------------------------------------
	Function Name : AuthorizationFailure
	Description : This functions perfoms authorization failure
	Arguments : event, rc, prc
	Return Type : JSON
	----------------------------------------------------------------------------------*/
   public void function AuthorizationFailure(event, rc, prc)
   {
       onAuthorizationFailure(event, rc, prc);
   }
}