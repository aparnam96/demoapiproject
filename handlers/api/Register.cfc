/**
 * Register
 *
 * @author aparna
 * @date 11/3/19
 **/
component displayname="Employee"
          hint="This is a handler for Register the api user."
          extends="handlers.BaseHandler"{

	property name="loginService" inject;

    //Rest Allowed methods
    this.allowedMethods = {
		RegisterApiUser = 'POST'

	};
	/*----------------------------------------------------------------------------------
	Function Name :  RegisterApiUser
	Description : Register the users
	Arguments : event,rc,prc
	Return Type : Json
	----------------------------------------------------------------------------------*/

	public void function RegisterApiUser(event, rc, prc)
	{
		var isRegistered = loginService.RegisterUser(rc.FirstName, rc.LastName, rc.Email, rc.Password);
		prc.response.setData("").addMessage(isRegistered.message).setError(isRegistered.error);
	}

}